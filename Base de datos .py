import sqlite3
'''
conexion=sqlite3.connect("LABASEDEDATOS")
try:    
    conexion.execute("""CREATE TABLE IF NOT EXISTS Vehiculo( 
                             idVehiculo integer primary key autoincrement,
                             PlacaVehiculo real not null,
                             DescripcionVehiculo varchar(50) not null,
                             EstadoVehiculo varchar(50) not null,
                             FOREIGN KEY (idDomicilio)
                             REFERENCES Domiciliario(idDomicilio)
                             FOREIGN KEY (NombreDomicilio)
                             REFERENCES Domiciliario(NombreDomicilio)
                             FOREIGN KEY (EstadoDomicilio)
                             REFERENCES Domiciliario(EstadoDomicilio)  
                        )""")

    conexion.execute("""CREATE TABLE IF NOT EXISTS Domiciliario(
                             idDomicilio integer primary key autoincrement,
                             NombreDomicilio varchar(50) not null,
                             TipoDocCDomicilio varchar(30) not null,
                             DocumentoDomicilio int not null,
                             CelDomicilio int not null,
                             EstadoDomicilio varchar(50) not null,
                             FOREIGN KEY (idVehiculo)
                             REFERENCES Vehiculo(idVehiculo)
                             FOREIGN KEY (PlacaVehiculo)
                             REFERENCES Vehiculo (PlacaVehiculo)
                             FOREIGN KEY (EstadoVehiculo)
                             REFERENCES Vehiculo (EstadoVehiculo)                
                        )""")
    

    conexion.execute("""CREATE TABLE IF NOT EXISTS Proveedores(  
                             idProveedor integer primary key autoincrement,
                             NombreProveedor varchar(50) not null,
                             CelProveedor int not null,
                             DireccionProveedor varchar(50) not null,
                             ProductoProveedor varchar(50) not null,
                             ValorProveedor real not null,
                             EmpresaProveedor varchar(50) not null,
                             RelevanciaProveedor varchar(50) not null   
                        )""")
                        
    conexion.execute("""CREATE TABLE IF NOT EXISTS Clientes (
                              idCliente integer primary key autoincrement,
                              NombreCliente varchar(50) not null,
                              ApellidoCliente varchar(50) not null,
                              TipoDocumento varchar(30) not null,
                              Documentoidentidad int not null,
                              CelCliente int not null,
                              DescripcionProducto varchar(50) not null,
                              valorProducto real not null,
                              DireccionProducto varchar(50) not null,
                              idProducto integer primary key autoincrement
                        )""")
                        
    conexion.execute("""CREATE TABLE IF NOT EXISTS Productos (
                              IdProducto integer primary key autoincrement,
                              NombreProducto varchar(50) not null,
                              DescripcionProducto varchar(50) not null,
                              valorProducto real not null,
                              fechaProducto datetime not null,
                              DireccionProducto varchar(50) not null
                        )""")
    print("se creo la tabla PROD")                      
except sqlite3.OperationalError:
    print("La tabla PROD ya existe") 
    
'''   
 
''' 
    #insertar datos
conexion=sqlite3.connect("LABASEDEDATOS")
conexion.execute("insert into Clientes (idClientes,NombreCliente,ApellidoCliente,TipoDocumento,Documentoidentidad,CelCliente,DescripcionProducto,valorProducto,DireccionProducto,idProducto) values (?,?,?,?,?,?,?,?,?,?)",
                     (1,"Sebastian","Jimenez","CC", 20211025117, 3125726273, "tapete decorado 1",70000,"cra20 10-30sur",1))
conexion.execute("insert into Clientes (idClientes,NombreCliente,ApellidoCliente,TipoDocumento,Documentoidentidad,CelCliente,DescripcionProducto,valorProducto,DireccionProducto, idProducto) values (?,?,?,?,?,?,?,?,?,?)",
                     (2,"Sebastian","Cantor","CC", 20211025077, 31330562255, "tapete decorado 2",70500,"cra20 10-30sur",2))
conexion.commit()
print("h")
conexion.close()
  
conexion=sqlite3.connect("LABASEDEDATOS")
conexion.execute("insert into Productos (NombreProducto,DescripcionProducto,valorProducto,fechaProducto,DireccionProducto) values (?,?,?,?,?)", ("tapete decorado 1","tapete decorado con pepas doradas de mentiras",70000, 11/8/22,"cra20 10-30sur"))
conexion.execute("insert into Productos (NombreProducto,DescripcionProducto,valorProducto,fechaProducto,DireccionProducto) values (?,?,?,?,?)", ("tapete decorado 2","tapete decorado con pepas plateadas de plata",70500, 11/8/22,"cra20 10-30sur"))
conexion.commit()
print ("x")
conexion.close()
'''
#eliminar datos
conexion=sqlite3.connect("LABASEDEDATOS")
conexion.execute("DELETE FROM Clientes WHERE idClientes < 15")
conexion.execute("DELETE FROM Productos WHERE valorProducto < 100000000")
conexion.commit()
print("y")
conexion.close()